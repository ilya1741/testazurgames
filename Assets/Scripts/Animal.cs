﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Animal : MonoBehaviour
{
    [SerializeField] private ParticleSystem _particle;
    private GameObject _target;
    private float _speed;
    private Rigidbody _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (_target != null)
        {
            gameObject.transform.LookAt(_target.transform.position);
            _rigidbody.velocity = transform.forward * _speed * GameManager.Instance.GameSpeed;
        }
        transform.position = new Vector3(transform.position.x, 1, transform.position.z);
    }

    public void SetTarget(GameObject target)
    {
        _target = target;
    }

    public void SetSpeed(float speed)
    {
        _speed = speed;
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject == _target)
        {
            ParticleSystem particle = Instantiate(_particle, _target.transform.position, Quaternion.identity);
            particle.Play();
            Destroy(_target);
            GameManager.Instance.FoodConsumed(this);
            Destroy(particle, 1f);
        }
    }
}
