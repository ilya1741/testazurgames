﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class UIManager : MonoBehaviour
{
    [SerializeField] private Button _newGame;
    [SerializeField] private Button _savedGame;
    [SerializeField] private Button _startGame;
    [SerializeField] private Button _savedInfo;

    [SerializeField] private Slider _gameFieldSize;
    [SerializeField] private Slider _animalsCount;
    [SerializeField] private Slider _animalsSpeed;
    [SerializeField] private Slider _cameraEuler;
    [SerializeField] private Slider _timeScale;

    [SerializeField] private GameObject _chooseGame;
    [SerializeField] private GameObject _initialSetup;
    [SerializeField] private GameObject _inGameUi;

    [SerializeField] private Text _gameFieldSizeText;
    [SerializeField] private Text _animalsCountText;
    [SerializeField] private Text _animalsSpeedText;
    [SerializeField] private Text _timeScaleText;
    [SerializeField] private Text _cameraEulerText;

    private float _gameFieldSizeValue;
    private float _animalsCountValue;
    private float _animalsSpeedValue;

    private UIState _currentUIState;


    private void OnEnable()
    {
        Subscribe();
        SetInitialValues();
        SetState(UIState.SelectMode);
    }

    private void OnDisable()
    {
        Unsubscribe();
    }

    private void UpdateMaxSliderValues()
    {
        _animalsCount.maxValue = MaximumAnimalCount();
    }

    private void SetInitialValues()
    {
        _cameraEuler.value = GameManager.Instance.CameraRotation;
        _cameraEulerText.text = _cameraEuler.value.ToString();

        _timeScale.value = 1;
        _timeScaleText.text = _timeScale.value.ToString();

        _gameFieldSizeValue = _gameFieldSize.value;
        _gameFieldSizeText.text = _gameFieldSize.value.ToString();

        _animalsSpeedValue = _animalsSpeed.value;
        _animalsSpeedText.text = _animalsSpeed.value.ToString();

        _animalsCountValue = _animalsCount.value;
        _animalsCountText.text = ((int)_animalsCount.value).ToString();

    }

    #region BUTTON HANDLERS

    private void NewGame()
    {
        SetState(UIState.InitialSettings);
    }

    private void LoadSavedGame()
    {
        GameManager.Instance.LoadGame();
        SetState(UIState.Game);
    }

    private void StartGame()
    {
        GameManager.Instance.Initialize(_animalsSpeedValue, Convert.ToInt32(_animalsCountValue), _gameFieldSizeValue);
        GameManager.Instance.SpawnAnimalAndFood();
        SetState(UIState.Game);
    }


    #endregion

    #region SLIDERS HANDLERS

    private void SetGameFieldSize(float val)
    {
        UpdateMaxSliderValues();
        if (MinimumGameFieldSize() > _gameFieldSize.value)
        {
            _gameFieldSize.value = MinimumGameFieldSize();
        }

        _gameFieldSizeText.text = val.ToString();
        _gameFieldSizeValue = val;

    }
    private void SetAnimalsCount(float val)
    {
        UpdateMaxSliderValues();
        if (MaximumAnimalCount() < _animalsCount.value)
        {
            _animalsCount.value = MaximumAnimalCount();
        }

        _animalsCountText.text = ((int)val).ToString();
        _animalsCountValue = val;
    }
    private void SetCameraEuler(float val)
    {
        _cameraEulerText.text = val.ToString();
        GameManager.Instance.SetCameraRotation(val);
    }
    private void SetAnimalsSpeed(float val)
    {
        _animalsSpeedText.text = val.ToString();
        _animalsSpeedValue = val;
    }
    private void SetApplicationSpeed(float val)
    {
        GameManager.Instance.SetGameSpeed(val);
        _timeScaleText.text = val.ToString();
    }

    #endregion

    #region UIStateMachine

    private enum UIState
    {
        SelectMode,
        InitialSettings,
        Game
    }

    private void SetState(UIState state)
    {
        _currentUIState = state;
        UpdateState();
    }

    private void UpdateState()
    {
        _chooseGame.SetActive(_currentUIState == UIState.SelectMode);
        _initialSetup.SetActive(_currentUIState == UIState.InitialSettings);
        _inGameUi.SetActive(_currentUIState == UIState.Game);
    }

    #endregion

    #region Subscribe-Unsubscribe

    private void Subscribe()
    {
        _savedInfo.onClick.AddListener(() =>
        {
            GameManager.Instance.SaveGame();
        });
        _newGame.onClick.AddListener(NewGame);
        _savedGame.onClick.AddListener(LoadSavedGame);
        _startGame.onClick.AddListener(StartGame);
        _gameFieldSize.onValueChanged.AddListener(SetGameFieldSize);
        _animalsCount.onValueChanged.AddListener(SetAnimalsCount);
        _animalsSpeed.onValueChanged.AddListener(SetAnimalsSpeed);
        _timeScale.onValueChanged.AddListener(SetApplicationSpeed);
        _cameraEuler.onValueChanged.AddListener(SetCameraEuler);

    }

    private void Unsubscribe()
    {
        _savedInfo.onClick.RemoveListener(() =>
        {
            GameManager.Instance.SaveGame();
        });
        _newGame.onClick.RemoveListener(NewGame);
        _savedGame.onClick.RemoveListener(LoadSavedGame);
        _startGame.onClick.RemoveListener(StartGame);
        _gameFieldSize.onValueChanged.RemoveListener(SetGameFieldSize);
        _animalsCount.onValueChanged.RemoveListener(SetAnimalsCount);
        _animalsSpeed.onValueChanged.RemoveListener(SetAnimalsSpeed);
        _timeScale.onValueChanged.RemoveListener(SetApplicationSpeed);
        _cameraEuler.onValueChanged.RemoveListener(SetCameraEuler);
    }

    #endregion

    #region Clampers

    private float MinimumGameFieldSize()
    {
        return Mathf.Sqrt(_animalsCountValue * 2);
    }

    private int MaximumAnimalCount()
    {
        return (int)((_gameFieldSizeValue * _gameFieldSizeValue) / 2);
    }

    #endregion

}
