﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using Newtonsoft.Json;
using UnityEngine;
using Random = UnityEngine.Random;

struct AnimalInfo
{
    public int TypeIndex;
    public GameObject Food;
}

class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;

    public float Size { get; private set; }
    public float GameSpeed { get; private set; } = 1;
    public float CameraRotation { get; private set; } = 75;

    [SerializeField] private Animal[] _animals;
    [SerializeField] private GameObject[] _animalFood;
    [SerializeField] private Material floorMaterial;

    private Dictionary<Animal, AnimalInfo> animalsAndFoods = new Dictionary<Animal, AnimalInfo>();
    private int _animalsCount;
    private float _speed;
    private float _radius;
    private RaycastHit _hit;

    private const string SAVED_GAME_KEY = "SAVED_GAME_KEY";
    private const string SLIDER_INFO_KEY = "INITIAL_SLIDER_VALUES";

    public void FoodConsumed(Animal animal)
    {
        int foodType = animalsAndFoods.First(x => x.Key == animal).Value.TypeIndex;
        StartCoroutine(SpawnAndSetFood(animal, false, foodType));
    }

    private void CreateField(float fieldSize)
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = Vector3.zero;
        cube.transform.localScale = new Vector3(fieldSize, 1, fieldSize);
        cube.AddComponent<BoxCollider>();
        cube.GetComponent<Renderer>().material = floorMaterial;

    }

    #region INITIALIZE AND SET

    public void SpawnAnimalAndFood()
    {
        StartCoroutine(SpawnAnimalAndFoodRoutine());
    }

    public void SetGameSpeed(float gameSpeed)
    {
        GameSpeed = gameSpeed;
    }

    public void SetCameraRotation(float cameraRotation)
    {
        CameraRotation = cameraRotation;
    }

    public void Initialize(float speed, int numberOfAnimals, float fieldSize)
    {
        _animalsCount = numberOfAnimals;
        Size = fieldSize;
        _speed = speed;
        _radius = speed * 5;
        CreateField(fieldSize);
    }

    #endregion

    #region LOAD SAVE GAME
    public void SaveGame()
    {
        InitialSavedInfo savedSliderInfo = new InitialSavedInfo
        {
            Speed = _speed,
            AnimalCount = _animalsCount,
            FieldSize = Size
        };

        string initialInfo = JsonUtility.ToJson(savedSliderInfo);

        PlayerPrefs.SetString(SLIDER_INFO_KEY, initialInfo);

        List<SavedInfo> _savedInfo = new List<SavedInfo>();

        foreach (var animalAndFood in animalsAndFoods)
        {
            if (animalAndFood.Key == null || animalAndFood.Value.Food == null)
            {
                continue;
            }
            SavedInfo localSavedInfo = new SavedInfo
            {
                PositionAnimal = animalAndFood.Key.gameObject.transform.position,
                PositionFood = animalAndFood.Value.Food.transform.position,
                TypeIndex = animalAndFood.Value.TypeIndex

            };
            _savedInfo.Add(localSavedInfo);
        }

        string savedGame = JsonConvert.SerializeObject(_savedInfo);

        PlayerPrefs.SetString(SAVED_GAME_KEY, savedGame);

    }

    public void LoadGame()
    {
        string savedGame = PlayerPrefs.GetString(SAVED_GAME_KEY, "");
        string savedSliderInfo = PlayerPrefs.GetString(SLIDER_INFO_KEY, "");
        if (string.IsNullOrEmpty(savedSliderInfo) || string.IsNullOrEmpty(savedGame))
        {
            return;
        }
        List<SavedInfo> loadedGame = JsonConvert.DeserializeObject<List<SavedInfo>>(savedGame);

        InitialSavedInfo loadedInitialInfo = JsonUtility.FromJson<InitialSavedInfo>(savedSliderInfo);

        Initialize(loadedInitialInfo.Speed, loadedInitialInfo.AnimalCount, loadedInitialInfo.FieldSize);
        StartCoroutine(SpawnAnimalAndFoodRoutineFromLoadedGame(loadedGame));

    }

    private IEnumerator SpawnAnimalAndFoodRoutineFromLoadedGame(List<SavedInfo> loadedGame)
    {

        foreach (var loadedInfo in loadedGame)
        {
            yield return null;
            var animalPos = loadedInfo.PositionAnimal;
            var foodPos = loadedInfo.PositionFood;
            var index = loadedInfo.TypeIndex;

            Animal animal = Instantiate(_animals[index], animalPos, Quaternion.identity);

            GameObject animalFood = Instantiate(_animalFood[index], foodPos, Quaternion.identity);// Спауним там еду

            animalsAndFoods.Add(animal, new AnimalInfo()
            {
                Food = animalFood,
                TypeIndex = index
            });

            animal.SetTarget(animalFood);
            animal.SetSpeed(_speed);
        }
    }



    #endregion

    #region SPAWN PROCESS

    private IEnumerator SpawnAnimalAndFoodRoutine()
    {

        for (int i = 0; i < _animalsCount; i++)
        {
            yield return null;
            int count = _animals.Length;
            int typeFood = Random.Range(0, count);

            float animalRandomX = Random.Range(Size / 2, -Size / 2);
            float animalRandomZ = Random.Range(Size / 2, -Size / 2);


            var animalPos = new Vector3(animalRandomX, 0.5f, animalRandomZ);

            Animal animal = Instantiate(_animals[typeFood], animalPos, Quaternion.identity);
            animal.SetSpeed(_speed);
            StartCoroutine(SpawnAndSetFood(animal, true, typeFood));



        }
    }

    private IEnumerator SpawnAndSetFood(Animal animal, bool isAddToDictionary, int typeFood)
    {
        while (true)
        {
            Vector3 foodPos = GetMeNewCoordinatesForSpawnFood(animal);
            bool canSpawn = CanSpawnHere(foodPos);
            if (canSpawn)
            {
                GameObject animalFood = Instantiate(_animalFood[typeFood], foodPos, Quaternion.identity);// Спауним там еду
                if (isAddToDictionary)
                {
                    animalsAndFoods.Add(animal, new AnimalInfo()
                    {
                        Food = animalFood,
                        TypeIndex = typeFood
                    });
                }
                else
                {
                    animalsAndFoods[animal] = new AnimalInfo()
                    {
                        Food = animalFood,
                        TypeIndex = typeFood
                    };

                }
                animal.SetTarget(animalFood);
                yield break;
            }
            yield return null;
        }

    }

    private Vector3 GetMeNewCoordinatesForSpawnFood(Animal animal)
    {

        Vector3 edgeRightPoint = new Vector3(Size / 2, 0, animal.transform.position.z);//X = maximum
        Vector3 edgeLeftPoint = new Vector3(-Size / 2, 0, animal.transform.position.z);//X = minimum
        Vector3 edgeUpPoint = new Vector3(animal.transform.position.x, 0, Size / 2);//Z = maximum
        Vector3 edgeDownPoint = new Vector3(animal.transform.position.x, 0, -Size / 2);//Z = minumim

        float distanceRightXMaximum = Vector3.Distance(animal.transform.position, edgeRightPoint);//Расстояние до Х максимум
        float distanceLeftXMinimum = Vector3.Distance(animal.transform.position, edgeLeftPoint);//Расстояние до Х минимум
        float distanceUpZMaximum = Vector3.Distance(animal.transform.position, edgeUpPoint);//Расстояние до Z максимум
        float distanceDownZMinimum = Vector3.Distance(animal.transform.position, edgeDownPoint);//Расстояние до Z минимум


        float minimumRandomForX = animal.transform.position.x - _radius;

        if (_radius > distanceLeftXMinimum)
        {
            minimumRandomForX = animal.transform.position.x - distanceLeftXMinimum;
        }

        float maximumRandomForX = animal.transform.position.x + _radius;
        if (_radius > distanceRightXMaximum)
        {
            maximumRandomForX = animal.transform.position.x + distanceRightXMaximum;
        }

        float minimumRandomForZ = animal.transform.position.z - _radius;
        if (_radius > distanceDownZMinimum)
        {
            minimumRandomForZ = animal.transform.position.z - distanceDownZMinimum;
        }

        float maximumRandomForZ = animal.transform.position.z + _radius;
        if (_radius > distanceUpZMaximum)
        {
            maximumRandomForZ = animal.transform.position.z + distanceUpZMaximum;
        }

        float randomX = Random.Range(minimumRandomForX, maximumRandomForX);
        float randomZ = Random.Range(minimumRandomForZ, maximumRandomForZ);

        Vector3 foodPos = new Vector3(randomX, 1, randomZ);
        return foodPos;
    }
    private bool CanSpawnHere(Vector3 foodPos)
    {
        Ray ray = new Ray(foodPos + Vector3.up * 3, Vector3.down * 4);
        Physics.Raycast(ray, out _hit);
        if (_hit.collider == null)
        {
            return false;
        }
        if (_hit.collider.tag == "Food")
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    #endregion
  
    #region SINGLETON
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    #endregion
}
