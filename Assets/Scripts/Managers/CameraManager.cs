﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{

    [SerializeField] private float _scrollSpeed = 10.0f;
    [SerializeField] private float _zoomMin = 1.0f;
    [SerializeField] private int _mouseSpeed = 10;
    [SerializeField] private Transform _target;
    [SerializeField] private GameObject _cameraMover;
    [SerializeField] private float _initialDistance = 20;

    private float _distance;
    private Vector3 _position;

    private const int BORDER_OFFSET = 200;

    private void Awake()
    {
        SetInitialValues();
    }

    private void LateUpdate()
    {
        MoveCamera();
        RotateCamera();
        ZoomCamera();
    }

    private void SetInitialValues()
    {
        _distance = _initialDistance;
        CalculateAndSetCameraPosition();
    }

    private void MoveCamera()
    {
        if (Input.mousePosition.y > Screen.height - BORDER_OFFSET)
        {
            _cameraMover.transform.Translate(Vector3.forward * _mouseSpeed * Time.deltaTime);
        }
        if (Input.mousePosition.y < BORDER_OFFSET)
        {
            _cameraMover.transform.Translate(-Vector3.forward * _mouseSpeed * Time.deltaTime);
        }
        if (Input.mousePosition.x > Screen.width - BORDER_OFFSET)
        {
            _cameraMover.transform.Translate(Vector3.right * _mouseSpeed * Time.deltaTime);
        }
        if (Input.mousePosition.x < BORDER_OFFSET)
        {
            _cameraMover.transform.Translate(-Vector3.right * _mouseSpeed * Time.deltaTime);
        }
        ClampCameraPosition();
    }

    private void RotateCamera()
    {
        gameObject.transform.eulerAngles = new Vector3(GameManager.Instance.CameraRotation, transform.eulerAngles.y, transform.eulerAngles.z);
    }

    #region ZOOM
    private void ZoomCamera()
    {
        if (!(Math.Abs(Input.GetAxis("Mouse ScrollWheel")) > 0.01f))
        {
            return;
        }

        _distance = Vector3.Distance(transform.position, _target.position);
        _distance = ZoomLimit(_distance - Input.GetAxis("Mouse ScrollWheel") * _scrollSpeed, _zoomMin, GetMaxZoom());
        CalculateAndSetCameraPosition();
    }

    private float ZoomLimit(float dist, float min, float max)
    {
        if (dist < min)

            dist = min;

        if (dist > max)

            dist = max;

        return dist;
    }

    private float GetMaxZoom()
    {
        // Вычислено с помощью линейной регрессии на основе эмпирических значений. 
        float approxMaxDistance = (float)(GameManager.Instance.Size * 0.7261f + 21.7213);
        return approxMaxDistance;
    }

    #endregion

    #region COMMON CALCULATIONS

    private void CalculateAndSetCameraPosition()
    {
        _position = -(transform.forward * _distance) + _target.position;
        transform.position = _position;
    }

    private void ClampCameraPosition()
    {
        var floorSize = GameManager.Instance.Size;
        float topCorner = floorSize / 2;
        float downCorner = -floorSize / 2;

        if (_cameraMover.transform.localPosition.x > topCorner)
        {
            _cameraMover.transform.localPosition = new Vector3(topCorner, _cameraMover.transform.localPosition.y, _cameraMover.transform.localPosition.z);
        }
        if (_cameraMover.transform.localPosition.x < downCorner)
        {
            _cameraMover.transform.localPosition = new Vector3(downCorner, _cameraMover.transform.localPosition.y, _cameraMover.transform.localPosition.z);
        }

        if (_cameraMover.transform.localPosition.z > topCorner)
        {
            _cameraMover.transform.localPosition = new Vector3(_cameraMover.transform.localPosition.x, _cameraMover.transform.localPosition.y, topCorner);
        }
        if (_cameraMover.transform.localPosition.z < downCorner)
        {
            _cameraMover.transform.localPosition = new Vector3(_cameraMover.transform.localPosition.x, _cameraMover.transform.localPosition.y, downCorner);
        }
    }

    #endregion

}
