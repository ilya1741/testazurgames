﻿using UnityEngine;

namespace Assets.Scripts
{
    public class SavedInfo
    {
        public Vector3 PositionAnimal;
        public Vector3 PositionFood;
        public int TypeIndex;
    }
}
